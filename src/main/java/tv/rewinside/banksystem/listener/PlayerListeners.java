package tv.rewinside.banksystem.listener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.manager.UserManager;
import tv.rewinside.banksystem.misc.InventoryContainer;
import tv.rewinside.banksystem.utils.ScoreboardManager;

import java.util.Arrays;
import java.util.List;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class PlayerListeners implements Listener {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final UserManager userManager = plugin.getUserManager();
    private final InventoryContainer inventoryManager = plugin.getInventoryManager();
    private final ScoreboardManager scoreboardManager = plugin.getScoreboardManager();

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();

        userManager.createBankPlayer(player.getUniqueId());
        scoreboardManager.setScoreboard(plugin.getUserManager().getBankPlayer(player.getUniqueId()));
    }

    @EventHandler
    public void onPlayerConsumeItem(final PlayerItemConsumeEvent event) {
        if(event.getItem() == null)
            return;

        if(isFood(event.getItem().getType())) {
            final BankPlayer bankPlayer = plugin.getUserManager().getBankPlayer(event.getPlayer().getUniqueId());

            bankPlayer.removeMoneyAmountAsync(plugin.getSettings().getMeetEat(), currentMoney -> {
                event.getPlayer().sendMessage(plugin.getPrefix() + "Du hast nun " + currentMoney + " | - " + plugin.getSettings().getMeetEat() + " Euro");
            });
        }
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();

        if(!event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
            return;

        if(event.getClickedBlock() == null)
            return;

        if(event.getClickedBlock().getType() != Material.GOLD_BLOCK)
            return;

        player.openInventory(inventoryManager.getBankAccountsInventory(userManager.getBankPlayer(player.getUniqueId())));
    }

    private boolean isFood(Material material) {
        List<Material> materials = Arrays.asList(Material.COOKED_FISH, Material.COOKED_MUTTON, Material.MUTTON, Material.PORK, Material.GRILLED_PORK, Material.COOKED_BEEF, Material.RAW_BEEF);

        return materials.contains(material);
    }

}
