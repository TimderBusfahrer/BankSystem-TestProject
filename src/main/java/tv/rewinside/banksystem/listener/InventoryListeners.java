package tv.rewinside.banksystem.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class InventoryListeners implements Listener {

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if(event.getClickedInventory() == null)
            return;

        if(event.getCurrentItem() == null)
            return;

        if(event.getClickedInventory().getTitle().equals("Bank - Konten")) {
            event.setCancelled(true);
        }
    }

}
