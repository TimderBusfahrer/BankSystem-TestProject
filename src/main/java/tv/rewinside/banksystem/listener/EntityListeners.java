package tv.rewinside.banksystem.listener;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;

import java.util.Arrays;
import java.util.List;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class EntityListeners implements Listener {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    @EventHandler
    public void onEntityInteract(final PlayerInteractAtEntityEvent event) {
        final Player player = event.getPlayer();

        if(event.getRightClicked() == null)
            return;

        if(event.getPlayer().getItemInHand() == null)
            return;

        if(isAnimal(event.getRightClicked()) && player.getItemInHand().getType().equals(Material.WHEAT)) {
            final BankPlayer bankPlayer = plugin.getUserManager().getBankPlayer(player.getUniqueId());

            bankPlayer.addMoneyAmountAsync(plugin.getSettings().getAnimalFeed(), currentMoney -> {
                player.sendMessage(plugin.getPrefix() + "Du hast nun " + currentMoney + " | + " + plugin.getSettings().getAnimalFeed() + " Euro");
            });
        }
    }

    @EventHandler
    public void onEntityDeath(final EntityDeathEvent event) {
        final LivingEntity entity = event.getEntity();

        if(isMonster(entity)) {
            if(entity.getKiller() == null)
                return;

            final BankPlayer bankPlayer = plugin.getUserManager().getBankPlayer(entity.getKiller().getUniqueId());

            bankPlayer.addMoneyAmountAsync(plugin.getSettings().getMonsterKill(), currentMoney -> {
                event.getEntity().getKiller().sendMessage(plugin.getPrefix() + "Du hast nun " + currentMoney + " | + " + plugin.getSettings().getMonsterKill() + " Euro");
            });
        } else if(isAnimal(entity)) {
            if(entity.getKiller() == null)
                return;

            final BankPlayer bankPlayer = plugin.getUserManager().getBankPlayer(entity.getKiller().getUniqueId());

            bankPlayer.hasEnoughMoneyAsync(plugin.getSettings().getAnimalKill(), enough -> {
                if(enough) {
                    bankPlayer.removeMoneyAmountAsync(plugin.getSettings().getAnimalKill(), currentMoney -> {
                        event.getEntity().getKiller().sendMessage(plugin.getPrefix() + "Du hast nun " + currentMoney + " | - " + plugin.getSettings().getAnimalKill() + " Euro");
                    });
                }
            });
        }
    }

    private boolean isMonster(LivingEntity entity) {
        List<EntityType> entityTypes = Arrays.asList(EntityType.BAT, EntityType.BLAZE, EntityType.CAVE_SPIDER, EntityType.CREEPER, EntityType.ENDERMAN, EntityType.GHAST, EntityType.PIG_ZOMBIE, EntityType.SKELETON, EntityType.SPIDER, EntityType.ZOMBIE, EntityType.WITCH);

        return entityTypes.contains(entity.getType());
    }

    private boolean isAnimal(Entity entity) {
        List<EntityType> entityTypes = Arrays.asList(EntityType.CHICKEN, EntityType.COW, EntityType.HORSE, EntityType.MUSHROOM_COW, EntityType.OCELOT, EntityType.PIG, EntityType.RABBIT, EntityType.SHEEP, EntityType.SQUID, EntityType.VILLAGER, EntityType.WOLF);

        return entityTypes.contains(entity.getType());
    }

}
