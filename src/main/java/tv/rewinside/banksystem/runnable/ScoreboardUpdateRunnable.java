package tv.rewinside.banksystem.runnable;

import org.bukkit.scoreboard.Scoreboard;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.utils.ScoreboardManager;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class ScoreboardUpdateRunnable implements Runnable {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final ScoreboardManager scoreboardManager = plugin.getScoreboardManager();

    @Override
    public void run() {
        plugin.getServer().getOnlinePlayers().forEach(player -> {
            for(Scoreboard scoreboard : scoreboardManager.getSbupdate().keySet()) {
                scoreboard.getTeam("money").setPrefix("§2 §c" + scoreboardManager.getSbupdate().get(scoreboard).getMoneyAmount());
                scoreboard.getTeam("account").setPrefix(scoreboardManager.getSbupdate().get(scoreboard).getBankAccounts().size() == 1 ? "§4 §cEins" : "§4 §c" + scoreboardManager.getSbupdate().get(scoreboard).getBankAccounts().size());
                scoreboard.getTeam("allMoney").setPrefix("§4 §c" + plugin.getAccountManager().calcCompleteMoneyAmount(scoreboardManager.getSbupdate().get(scoreboard)));
            }
        });
    }
}
