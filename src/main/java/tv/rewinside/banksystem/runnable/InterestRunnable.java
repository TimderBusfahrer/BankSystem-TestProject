package tv.rewinside.banksystem.runnable;

import org.bukkit.entity.Player;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class InterestRunnable implements Runnable {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final double interest = plugin.getSettings().getInterestAmount();

    @Override
    public void run() {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            for (BankAccount bankAccount : plugin.getUserManager().getBankPlayer(player.getUniqueId()).getBankAccounts()) {
                bankAccount.setMoneyAmount((long) (bankAccount.getMoneyAmount() * interest));
            }
        }
    }

}
