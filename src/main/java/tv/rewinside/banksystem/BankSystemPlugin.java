package tv.rewinside.banksystem;

import com.google.gson.Gson;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;
import tv.rewinside.banksystem.command.KontoCommand;
import tv.rewinside.banksystem.command.MoneyCommand;
import tv.rewinside.banksystem.listener.EntityListeners;
import tv.rewinside.banksystem.listener.InventoryListeners;
import tv.rewinside.banksystem.listener.PlayerListeners;
import tv.rewinside.banksystem.manager.AccountManager;
import tv.rewinside.banksystem.manager.TransactionManager;
import tv.rewinside.banksystem.manager.UserManager;
import tv.rewinside.banksystem.misc.Config;
import tv.rewinside.banksystem.misc.InventoryContainer;
import tv.rewinside.banksystem.runnable.InterestRunnable;
import tv.rewinside.banksystem.runnable.ScoreboardUpdateRunnable;
import tv.rewinside.banksystem.utils.ConfigManager;
import tv.rewinside.banksystem.utils.MongoManager;
import tv.rewinside.banksystem.utils.ScoreboardManager;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

@Getter
public class BankSystemPlugin extends JavaPlugin {

    @Getter
    private static BankSystemPlugin instance;

    private String prefix;
    private Gson gson;

    private ConfigManager configManager;
    private MongoManager mongoManager;

    private Config settings;

    private UserManager userManager;
    private AccountManager accountManager;
    private TransactionManager transactionManager;

    private InventoryContainer inventoryManager;
    private ScoreboardManager scoreboardManager;

    @Override
    public void onLoad() {
        // Initialize the instance
        this.instance = this;

        // Initialize the prefix
        this.prefix = "§8[§cBankSystem§8] §7";
        // Initialize the Gson instance
        this.gson = new Gson();
        // Initialize the ConfigManager instance
        this.configManager = new ConfigManager();

        // Set the default Json-String in the settings.json
        this.configManager.write(new Config("localhost", 27017, "dbName", "root", "secretPassword", 5, 5, 3, 3, 1.12));
        // Initialize the MongoManager instance
        this.mongoManager = new MongoManager();

        // Initialize the UserManager instance
        this.userManager = new UserManager();

        // Initialize the AccountManager instance
        this.accountManager = new AccountManager();

        // Initialize the TransactionManager instance
        this.transactionManager = new TransactionManager();

        // Initialize the InventoryContainer instance
        this.inventoryManager = new InventoryContainer();

        // Initialize the ScoreboardManager instance
        this.scoreboardManager = new ScoreboardManager();
    }

    @Override
    public void onEnable() {
        init();
    }

    private void init() {
        // Initialize the Settings variable and get the data out of the settings.json file
        this.settings = configManager.read();
        // Connect to MongoDatabase & initialize the MongoCollections
        mongoManager.connect()
                .selectCollection();

        // Register the Listeners
        getServer().getPluginManager().registerEvents(new PlayerListeners(), this);
        getServer().getPluginManager().registerEvents(new EntityListeners(), this);
        getServer().getPluginManager().registerEvents(new InventoryListeners(), this);

        // Register the Commands
        getCommand("money").setExecutor(new MoneyCommand());
        getCommand("konto").setExecutor(new KontoCommand());

        // Runs the timer every 5 minutes
        getServer().getScheduler().runTaskTimerAsynchronously(this, new InterestRunnable(), 20 * 60 * 5, 20 * 60 * 5);

        // Runs the timer every 1/2 second
        getServer().getScheduler().runTaskTimerAsynchronously(this, new ScoreboardUpdateRunnable(), 0, 10L);
    }

}