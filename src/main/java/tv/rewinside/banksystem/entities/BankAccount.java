package tv.rewinside.banksystem.entities;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bson.Document;
import tv.rewinside.banksystem.BankSystemPlugin;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

@AllArgsConstructor
public class BankAccount {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final ExecutorService executorService = plugin.getMongoManager().getExecutorService();

    @Getter
    private int bankNumber;

    @Getter
    private UUID UUID;

    @Getter @Setter
    private long moneyAmount;

    /**
     *
     * A method to get the current money amount of a BankAccount
     *
     * @return the current amount of money
     * **/
    public long getMoneyAmount() {
        return plugin.getMongoManager().getBankAccounts().find(Filters.and(Filters.eq("_id", bankNumber), Filters.eq("UUID", UUID.toString()))).first().getLong("money");
    }

    /**
     *
     * A method to get the current money amount of a BankAccount asynchronously
     *
     * @return the current amount of money
     * **/
    public void getMoneyAmountAsync(Consumer<Long> consumer) {
        executorService.execute(() -> {
            consumer.accept(plugin.getMongoManager().getBankAccounts().find(Filters.and(Filters.eq("_id", bankNumber), Filters.eq("UUID", UUID.toString()))).first().getLong("money"));
        });
    }

    /**
     *
     * A method to add the money amount to the current money amount of a BankAccount
     *
     * @param money the amount of money to add
     * **/
    public long addMoneyAmount(long money) {
        plugin.getMongoManager().getBankAccounts().replaceOne(Filters.and(new Document("_id", bankNumber), new Document("UUID", UUID.toString())), new Document().append("_id", bankNumber).append("UUID", UUID.toString()).append("money", (getMoneyAmount() + money)), new UpdateOptions().upsert(true));
        return getMoneyAmount();
    }

    /**
     *
     * A method to add the money amount to the current money amount of a BankAccount asynchronously
     *
     * @param money the amount of money to add
     * @param consumer to return the current amount of money
     * **/
    public void addMoneyAmountAsync(long money, Consumer<Long> consumer) {
        executorService.execute(() -> {
            getMoneyAmountAsync(currentMoney -> {
                plugin.getMongoManager().getBankAccounts().replaceOne(Filters.and(new Document("_id", bankNumber), new Document("UUID", UUID.toString())), new Document().append("_id", bankNumber).append("UUID", UUID.toString()).append("money", (currentMoney + money)), new UpdateOptions().upsert(true));
                consumer.accept(currentMoney + money);
            });
        });
    }

    /**
     *
     * A method to set the current money amount of a BankAccount
     *
     * @param money the amount of money to set
     * **/
    public long setMoneyAmount(long money) {
        plugin.getMongoManager().getBankAccounts().replaceOne(Filters.and(new Document("_id", bankNumber), new Document("UUID", UUID.toString())), new Document().append("_id", bankNumber).append("UUID", UUID.toString()).append("money", money), new UpdateOptions().upsert(true));
        return money;
    }

    /**
     *
     * A method to set the current money amount of a BankAccount asynchronously
     *
     * @param money the amount of money to set
     * **/
    public void setMoneyAmountAsync(long money, Consumer<Long> consumer) {
        executorService.execute(() -> {
            plugin.getMongoManager().getBankAccounts().replaceOne(Filters.and(new Document("_id", bankNumber), new Document("UUID", UUID.toString())), new Document().append("_id", bankNumber).append("UUID", UUID.toString()).append("money", money), new UpdateOptions().upsert(true));
            consumer.accept(money);
        });
    }

    /**
     *
     * A method to remove the money amount to the current money amount of a BankAccount
     *
     * @param money the amount of money to remove
     * **/
    public long removeMoneyAmount(long money) {
        if((getMoneyAmount() - money) < 0)
            return 404;

        plugin.getMongoManager().getBankAccounts().replaceOne(Filters.and(new Document("_id", bankNumber), new Document("UUID", UUID.toString())), new Document().append("_id", bankNumber).append("UUID", UUID.toString()).append("money", (getMoneyAmount() - money)), new UpdateOptions().upsert(true));
        return getMoneyAmount();
    }

    /**
     *
     * A method to remove the money amount to the current money amount of a BankAccount asynchronously
     *
     * @param money the amount of money to remove
     * @param consumer to return the current amount of money
     * **/
    public void removeMoneyAmountAsync(long money, Consumer<Long> consumer) {
        executorService.execute(() -> {
            getMoneyAmountAsync(currentMoney -> {
                if((currentMoney - money) < 0)
                    consumer.accept((long) 404);

                plugin.getMongoManager().getBankAccounts().replaceOne(Filters.and(new Document("_id", bankNumber), new Document("UUID", UUID.toString())), new Document().append("_id", bankNumber).append("UUID", UUID.toString()).append("money", (getMoneyAmount() - money)), new UpdateOptions().upsert(true));
                consumer.accept(currentMoney - money);
            });
        });
    }

}
