package tv.rewinside.banksystem.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import tv.rewinside.banksystem.misc.TransactionType;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

@Getter
@AllArgsConstructor
public class BankTransaction {

    private String ID;
    private int bankNumber;
    private TransactionType type;
    private long moneyAmount;
    private long timestamp;

}
