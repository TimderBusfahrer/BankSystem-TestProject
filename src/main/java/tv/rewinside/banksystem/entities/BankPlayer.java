package tv.rewinside.banksystem.entities;

import com.mongodb.Block;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.bukkit.entity.Player;
import tv.rewinside.banksystem.BankSystemPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class BankPlayer {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final ExecutorService executorService = plugin.getMongoManager().getExecutorService();

    private final UUID uuid;

    public BankPlayer(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     *
     * A method to get the Bukkit Player
     *
     * @return the Bukkit Player
     * **/
    public Player getPlayer() {
        return plugin.getServer().getPlayer(uuid);
    }

    /**
     *
     * A method to get the current money amount of a BankPlayer
     *
     * @return the current amount of money
     * **/
    public long getMoneyAmount() {
        return plugin.getMongoManager().getBankPlayers().find(Filters.eq("_id", uuid.toString())).first().getLong("money");
    }

    /**
     *
     * A method to get the current money amount of a BankPlayer asynchronously
     *
     * @return the current amount of money
     * **/
    public void getMoneyAmountAsync(Consumer<Long> consumer) {
        executorService.execute(() -> {
            consumer.accept(plugin.getMongoManager().getBankPlayers().find(Filters.eq("_id", uuid.toString())).first().getLong("money"));
        });
    }

    /**
     *
     * A method to set the current money amount of a BankPlayer
     *
     * @param money the amount of money to set
     * **/
    public void setMoneyAmount(long money) {
        plugin.getMongoManager().getBankPlayers().replaceOne(Filters.eq("_id", uuid.toString()), new Document("_id", uuid.toString()).append("money", money), new UpdateOptions().upsert(true));
    }

    /**
     *
     * A method to set the current money amount of a BankPlayer asynchronously
     *
     * @param money the amount of money to set
     * **/
    public void setMoneyAmountAsync(long money) {
        executorService.execute(() -> {
            plugin.getMongoManager().getBankPlayers().replaceOne(Filters.eq("_id", uuid.toString()), new Document("_id", uuid.toString()).append("money", money), new UpdateOptions().upsert(true));
        });
    }

    /**
     *
     * A method to add the money amount to the current money amount of a BankPlayer
     *
     * @param money the amount of money to add
     * **/
    public long addMoneyAmount(long money) {
        plugin.getMongoManager().getBankPlayers().replaceOne(Filters.eq("_id", uuid.toString()), new Document("_id", uuid.toString()).append("money", (getMoneyAmount() + money)), new UpdateOptions().upsert(true));
        return getMoneyAmount();
    }

    /**
     *
     * A method to add the money amount to the current money amount of a BankPlayer asynchronously
     *
     * @param money the amount of money to add
     * @param consumer to return the current amount of money
     * **/
    public void addMoneyAmountAsync(long money, Consumer<Long> consumer) {
        executorService.execute(() -> {
            getMoneyAmountAsync(currentMoney -> {
                plugin.getMongoManager().getBankPlayers().replaceOne(Filters.eq("_id", uuid.toString()), new Document("_id", uuid.toString()).append("money", (currentMoney + money)), new UpdateOptions().upsert(true));
                consumer.accept(currentMoney + money);
            });
        });
    }

    /**
     *
     * A method to remove the money amount to the current money amount of a BankPlayer
     *
     * @param money the amount of money to remove
     * **/
    public long removeMoneyAmount(long money) {
        if((getMoneyAmount() - money) < 0)
            return 404;

        plugin.getMongoManager().getBankPlayers().replaceOne(Filters.eq("_id", uuid.toString()), new Document("_id", uuid.toString()).append("money", (getMoneyAmount() - money)), new UpdateOptions().upsert(true));
        return getMoneyAmount();
    }

    /**
     *
     * A method to remove the money amount to the current money amount of a BankPlayer asynchronously
     *
     * @param money the amount of money to remove
     * @param consumer to return the current amount of money
     * **/
    public void removeMoneyAmountAsync(long money, Consumer<Long> consumer) {
        executorService.execute(() -> {
            getMoneyAmountAsync(currentMoney -> {
                if((currentMoney - money) < 0)
                    consumer.accept((long) 404);

                plugin.getMongoManager().getBankPlayers().replaceOne(Filters.eq("_id", uuid.toString()), new Document("_id", uuid.toString()).append("money", (currentMoney - money)), new UpdateOptions().upsert(true));
                consumer.accept(currentMoney - money);
            });
        });
    }

    /**
     *
     * Check if the BankPlayer has enough money
     *
     * @param money the amount of money to check
     * @return a boolean if the money is enough
     * **/
    public boolean hasEnoughMoney(long money) {
        return getMoneyAmount() >= money;
    }

    /**
     *
     * Check if the BankPlayer has enough money
     *
     * @param money the amount of money to check
     * @param consumer return a boolean if the money is enough
     * **/
    public void hasEnoughMoneyAsync(long money, Consumer<Boolean> consumer) {
        getMoneyAmountAsync(currentMoney -> {
            consumer.accept(currentMoney >= money);
        });
    }

    /**
     *
     * A method to get all BankAccounts of a BankPlayer
     *
     * @return a list of BankAccounts
     * **/
    public List<BankAccount> getBankAccounts() {
        List<BankAccount> bankAccounts = new ArrayList<>();

        plugin.getMongoManager().getBankAccounts().find(Filters.eq("UUID", uuid.toString())).forEach((Block<? super Document>) document -> {
            bankAccounts.add(new BankAccount(document.getInteger("_id"), UUID.fromString(document.getString("UUID")), document.getLong("money")));
        });

        return bankAccounts;
    }

    /**
     *
     * A method to get all BankAccounts of a BankPlayer asynchronously
     *
     * @param consumer return a list of BankAccounts
     * **/
    public void getBankAccountsAsync(Consumer<List<BankAccount>> consumer) {
        executorService.execute(() -> {
            List<BankAccount> bankAccounts = new ArrayList<>();

            plugin.getMongoManager().getBankAccounts().find(Filters.eq("UUID", uuid.toString())).forEach((Block<? super Document>) document -> {
                bankAccounts.add(new BankAccount(document.getInteger("_id"), UUID.fromString(document.getString("UUID")), document.getLong("money")));
            });

            consumer.accept(bankAccounts);
        });
    }

}
