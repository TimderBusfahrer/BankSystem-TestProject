package tv.rewinside.banksystem.misc;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class ItemBuilder {

    private String name = "";
    private Material material = Material.AIR;
    private int amount = 1;
    private int damage = 0;
    private int subid = 0;
    private List<String> lore = new ArrayList<>();

    public ItemBuilder (Material material) {
        this.material = material;
    }

    public ItemBuilder setDisplayName(String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder setStackAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder setItemDamage(int damage) {
        this.damage = damage;
        return this;
    }

    public ItemBuilder setLore(List<String> list) {
        this.lore = list;
        return this;
    }

    public ItemBuilder setSubID(int subid) {
        this.subid = subid;
        return this;
    }

    public ItemStack build() {
        ItemStack item = new ItemStack(material, amount, (short) subid);

        item.setDurability((short) damage);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }


}
