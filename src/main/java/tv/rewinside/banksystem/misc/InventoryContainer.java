package tv.rewinside.banksystem.misc;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.manager.AccountManager;

import java.util.Arrays;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class InventoryContainer {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final AccountManager accountManager = plugin.getAccountManager();

    public Inventory getBankAccountsInventory(BankPlayer player) {
        Inventory inventory = plugin.getServer().createInventory(null, 54, "Bank - Konten");

        player.getBankAccountsAsync(bankAccounts -> bankAccounts.forEach(bankAccount -> {
            inventory.addItem(new ItemBuilder(Material.BOOK).setDisplayName("§7Kontonummer: " + bankAccount.getBankNumber()).setLore(Arrays.asList("§7Guthaben: " + bankAccount.getMoneyAmount(), "§7Transaktionen: " + accountManager.getBankTransactions(bankAccount).size())).build());
        }));
        return inventory;
    }

}
