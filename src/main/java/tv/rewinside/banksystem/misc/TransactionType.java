package tv.rewinside.banksystem.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

@AllArgsConstructor
@Getter
public enum TransactionType {

    PLUS("Plus"),
    MINUS("Minus");

    private String name;

    public static TransactionType getType(String type) {
        for(TransactionType entry : TransactionType.values()) {
            if (entry.getName().equalsIgnoreCase(type)) {
                return entry;
            }
        }

        return null;
    }

}