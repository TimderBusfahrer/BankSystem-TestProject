package tv.rewinside.banksystem.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

@AllArgsConstructor
@Getter
public class Config {

    private String databaseHost;
    private int databasePort;
    private String databaseName;
    private String databaseUsername;
    private String databasePassword;

    private int animalFeed;
    private int monsterKill;
    private int animalKill;
    private int meetEat;

    private double interestAmount;

}