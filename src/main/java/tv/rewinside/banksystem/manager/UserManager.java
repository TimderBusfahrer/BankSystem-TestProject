package tv.rewinside.banksystem.manager;

import com.mongodb.client.model.CountOptions;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class UserManager {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final ExecutorService executorService = plugin.getMongoManager().getExecutorService();

    /**
     * Checks if the BankPlayer exists
     *
     * @param uuid of the BankPlayer Object
     * @return a boolean of BankPlayer is existing
     * **/
    public boolean existsBankPlayer(UUID uuid) {
        return (plugin.getMongoManager().getBankPlayers().count(Filters.eq("_id", uuid.toString()), new CountOptions().limit(1)) != 0);
    }

    /**
     * Checks asynchronously if the BankPlayer exists
     *
     * @param uuid of the BankPlayer Object
     * @param consumer give a boolean of BankPlayer is existing
     * **/
    public void existsBankPlayerAsync(UUID uuid, Consumer<Boolean> consumer) {
        executorService.execute(() -> {
            consumer.accept((plugin.getMongoManager().getBankPlayers().count(Filters.eq("_id", uuid.toString()), new CountOptions().limit(1)) != 0));
        });
    }

    /**
     * Create the BankPlayer in MongoDB if Player isn't existing
     *
     * @param uuid of the BankPlayer Object
     * @return a BankPlayer Object
     * **/
    public BankPlayer createBankPlayer(UUID uuid) {
        if(!existsBankPlayer(uuid))
            plugin.getMongoManager().getBankPlayers().insertOne(new Document("_id", uuid.toString()).append("money", new Long(0)));

        return new BankPlayer(uuid);
    }

    /**
     * Create the BankPlayer asynchronously in MongoDB if Object isn't existing
     *
     * @param uuid of the BankPlayer Object
     * @param consumer returns a BankPlayer Object
     * **/
    public void createBankPlayerAsync(UUID uuid, Consumer<BankPlayer> consumer) {
        existsBankPlayerAsync(uuid, exist -> {
            if(!exist)
                executorService.execute(() -> {
                    plugin.getMongoManager().getBankPlayers().insertOne(new Document("_id", uuid.toString()).append("money", new Long(0)));
                    consumer.accept(new BankPlayer(uuid));
                });
        });
    }

    /**
     * Returns a BankPlayer Object
     *
     * @param uuid of the BankPlayer Object
     * @return a BankPlayer Object
     * **/
    public BankPlayer getBankPlayer(UUID uuid) {
        return new BankPlayer(uuid);
    }

}
