package tv.rewinside.banksystem.manager;

import org.apache.commons.lang.RandomStringUtils;
import org.bson.Document;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;
import tv.rewinside.banksystem.misc.TransactionType;

import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class TransactionManager {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final ExecutorService executorService = plugin.getMongoManager().getExecutorService();

    /**
     * Checks if the Transaction ID is existing
     *
     * @param ID to get the BankTransactions
     * **/
    private boolean existsTransactionID(String ID) {
        for (Document document : plugin.getMongoManager().getBankTransactions().find()) {
            if(document.getString("_id").equals(ID)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks asynchronously if the Transaction ID is existing
     *
     * @param ID to get the BankTransactions
     * **/
    private void existsTransactionIDAsync(String ID, Consumer<Boolean> consumer) {
        executorService.execute(() -> {
            for (Document document : plugin.getMongoManager().getBankTransactions().find()) {
                if(document.getString("_id").equals(ID)) {
                    consumer.accept(true);
                }
            }

            consumer.accept(false);
        });
    }

    /**
     * Adds BankTransaction to the MongoDB
     *
     * @param bankAccount to get the BankAccount datas
     * @param type is the Type of the Transaction (Plus | Minus)
     * @param moneyAmount is the amount of money witch got moved
     * **/
    public void addTransaction(BankAccount bankAccount, TransactionType type, long moneyAmount) {
        String transactionID = RandomStringUtils.randomAlphanumeric(20);

        while (existsTransactionID(transactionID)) {
            transactionID = RandomStringUtils.randomAlphanumeric(20);
        }

        plugin.getMongoManager().getBankTransactions().insertOne(new Document("_id", transactionID).append("bankNumber", bankAccount.getBankNumber()).append("type", type.getName()).append("money", moneyAmount).append("timestamp", System.currentTimeMillis()));
    }

    /**
     * Adds BankTransaction asynchronously to the MongoDB
     *
     * @param bankAccount to get the BankAccount datas
     * @param type is the Type of the Transaction (Plus | Minus)
     * @param moneyAmount is the amount of money witch got moved
     * **/
    public void addTransactionAsync(BankAccount bankAccount, TransactionType type, long moneyAmount) {
        executorService.execute(() -> {
            String transactionID = RandomStringUtils.randomAlphanumeric(20);

            while (existsTransactionID(transactionID)) {
                transactionID = RandomStringUtils.randomAlphanumeric(20);
            }

            plugin.getMongoManager().getBankTransactions().insertOne(new Document("_id", transactionID).append("bankNumber", bankAccount.getBankNumber()).append("type", type.getName()).append("money", moneyAmount).append("timestamp", System.currentTimeMillis()));
        });
    }

}
