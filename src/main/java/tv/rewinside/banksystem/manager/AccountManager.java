package tv.rewinside.banksystem.manager;

import com.mongodb.Block;
import com.mongodb.client.model.CountOptions;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.entities.BankTransaction;
import tv.rewinside.banksystem.misc.TransactionType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class AccountManager {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final ExecutorService executorService = plugin.getMongoManager().getExecutorService();

    /**
     * Checks if the BankAccount exists
     *
     * @param bankNumber of the BankAccount Object
     * @return a boolean of BankAccount is existing
     * **/
    public boolean existsBankAccount(int bankNumber) {
        return (plugin.getMongoManager().getBankAccounts().count(Filters.eq("_id", bankNumber), new CountOptions().limit(1)) != 0);
    }

    /**
     * Checks asynchronously if the BankAccount exists
     *
     * @param bankNumber of the BankAccount Object
     * @param consumer give a boolean of BankAccount is existing
     * **/
    public void existsBankAccountAsync(int bankNumber, Consumer<Boolean> consumer) {
        executorService.execute(() -> {
            consumer.accept(plugin.getMongoManager().getBankAccounts().count(Filters.eq("_id", bankNumber), new CountOptions().limit(1)) != 0);
        });
    }

    /**
     * Return the BankAccount Object
     *
     * @param player if the BankPlayer Object
     * @param bankNumber of the BankAccount Object
     * **/
    public BankAccount getBankAccount(BankPlayer player, int bankNumber) {
        for (BankAccount bankAccount : player.getBankAccounts()) {
            if(bankAccount.getBankNumber() == bankNumber) {
                return bankAccount;
            }
        }

        return null;
    }

    /**
     * Return the BankAccount Object asynchronously
     *
     * @param player if the BankPlayer Object
     * @param bankNumber of the BankAccount Object
     * @param consumer give the BankAccount Object
     * **/
    public void getBankAccountAsync(BankPlayer player, int bankNumber, Consumer<BankAccount> consumer) {
        player.getBankAccountsAsync(bankAccounts -> bankAccounts.forEach(bankAccount -> {
            if (bankAccount.getBankNumber() == bankNumber) {
                consumer.accept(bankAccount);
            }
        }));
    }

    /**
     * Creates a new BankAccount with an random ID
     *
     * @param bankAccount which one will be created in the MongoDB
     * **/
    public void createBankAccount(BankAccount bankAccount) {
        plugin.getMongoManager().getBankAccounts().insertOne(new Document("_id", bankAccount.getBankNumber()).append("UUID", bankAccount.getUUID().toString()).append("money", new Long(100)));
    }

    /**
     * Creates asynchronously a new BankAccount with an random ID
     *
     * @param bankAccount which one will be created in the MongoDB
     * **/
    public void createBankAccountAsync(BankAccount bankAccount) {
        executorService.execute(() -> {
            plugin.getMongoManager().getBankAccounts().insertOne(new Document("_id", bankAccount.getBankNumber()).append("UUID", bankAccount.getUUID().toString()).append("money", new Long(100)));
        });
    }

    /**
     * Remove the BankAccount from the MongoDB
     *
     * @param bankAccount which one will be removed in the MongoDB
     * **/
    public void removeBankAccount(BankAccount bankAccount) {
        plugin.getMongoManager().getBankAccounts().deleteOne(new Document("_id", bankAccount.getBankNumber()).append("UUID", bankAccount.getUUID().toString()).append("money", bankAccount.getMoneyAmount()));
    }

    /**
     * Remove the BankAccount asynchronously from the MongoDB
     *
     * @param bankAccount which one will be removed in the MongoDB
     * **/
    public void removeBankAccountAsync(BankAccount bankAccount) {
        executorService.execute(() -> {
            plugin.getMongoManager().getBankAccounts().deleteOne(new Document("_id", bankAccount.getBankNumber()).append("UUID", bankAccount.getUUID()).append("money", bankAccount.getMoneyAmount()));
        });
    }

    /**
     * Get all BankTransactions from a BankAccount
     *
     * @param bankAccount to get the BankTransactions
     * **/
    public List<BankTransaction> getBankTransactions(BankAccount bankAccount) {
        List<BankTransaction> bankTransactions = new ArrayList<>();

        plugin.getMongoManager().getBankTransactions().find(Filters.eq("bankNumber", bankAccount.getBankNumber())).forEach((Block<? super Document>) document -> {
            bankTransactions.add(new BankTransaction(document.getString("_id"), document.getInteger("bankNumber"), TransactionType.getType(document.getString("type")), document.getLong("money"), document.getLong("timestamp")));
        });

        return bankTransactions;
    }

    /**
     * Get all BankTransactions asynchronously from a BankAccount
     *
     * @param bankAccount to get the BankTransactions
     * **/
    public void getBankTransactionsAsync(BankAccount bankAccount, Consumer<List<BankTransaction>> consumer) {
        executorService.execute(() -> {
            List<BankTransaction> bankTransactions = new ArrayList<>();

            plugin.getMongoManager().getBankTransactions().find(Filters.eq("bankNumber", bankAccount.getBankNumber())).forEach((Block<? super Document>) document -> {
                bankTransactions.add(new BankTransaction(document.getString("_id"), document.getInteger("bankNumber"), TransactionType.getType(document.getString("type")), document.getLong("money"), document.getLong("timestamp")));
            });

            consumer.accept(bankTransactions);
        });
    }

    /**
     * Get all BankAccounts from the BankPlayer
     *
     * @param player to get all BankAccounts
     * **/
    public long calcCompleteMoneyAmount(BankPlayer player) {
        long toReturn = 0;

        for(BankAccount bankAccount : player.getBankAccounts()) {
            toReturn = (toReturn + bankAccount.getMoneyAmount());
        }

        return toReturn;
    }

}
