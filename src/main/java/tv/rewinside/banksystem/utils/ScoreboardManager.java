package tv.rewinside.banksystem.utils;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class ScoreboardManager {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    @Getter
    private final Map<Scoreboard, BankPlayer> sbupdate = new HashMap<>();
    private Scoreboard scoreboard;

    public void setScoreboard(BankPlayer player) {
        org.bukkit.scoreboard.ScoreboardManager manager = this.plugin.getServer().getScoreboardManager();
        scoreboard = manager.getNewScoreboard();

        Objective objective = scoreboard.registerNewObjective("aaa", "bbb");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§cRewinside §8| §7BankSystem");

        Team money = scoreboard.registerNewTeam("money");
        money.setPrefix("§2 §c" + player.getMoneyAmount());
        money.addEntry(ChatColor.AQUA.toString());

        Team account = scoreboard.registerNewTeam("account");
        account.setPrefix(player.getBankAccounts().size() == 1 ? "§4 §cEins" : "§4 §c" + player.getBankAccounts().size());
        account.addEntry(ChatColor.BLACK.toString());

        Team allMoney = scoreboard.registerNewTeam("allMoney");
        allMoney.setPrefix("§4 §c" + this.plugin.getAccountManager().calcCompleteMoneyAmount(player));
        allMoney.addEntry(ChatColor.BLUE.toString());

        objective.getScore("§1").setScore(16);
        objective.getScore("§7Bargeld:").setScore(15);
        objective.getScore(ChatColor.AQUA.toString()).setScore(14);
        objective.getScore("§3").setScore(13);
        objective.getScore("§7Konten:").setScore(12);
        objective.getScore(ChatColor.BLACK.toString()).setScore(11);
        objective.getScore("§5").setScore(10);
        objective.getScore("§7Gesamtguthaben:").setScore(9);
        objective.getScore(ChatColor.BLUE.toString()).setScore(8);
        objective.getScore("§6").setScore(7);

        sbupdate.put(scoreboard, player);

        player.getPlayer().setScoreboard(scoreboard);
    }

}
