package tv.rewinside.banksystem.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.misc.Config;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class ConfigManager {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    private final Gson gson;
    private final File folder;

    public ConfigManager() {
        this.gson = new Gson();
        this.folder = this.plugin.getDataFolder();

        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    public void write(Config config) {
        try {
            File file = new File(this.folder + "/settings.json");

            if (!file.exists()) {
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(gson.toJson(config));
                fileWriter.flush();
                fileWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Config read() {
        for (File file : folder.listFiles()) {
            try {
                JsonParser jsonParser = new JsonParser();
                JsonObject parsedObject = (JsonObject) jsonParser.parse(new FileReader(file.getPath()));

                return new Config(
                        parsedObject.get("databaseHost").getAsString(),
                        parsedObject.get("databasePort").getAsInt(),
                        parsedObject.get("databaseName").getAsString(),
                        parsedObject.get("databaseUsername").getAsString(),
                        parsedObject.get("databasePassword").getAsString(),
                        parsedObject.get("animalFeed").getAsInt(),
                        parsedObject.get("monsterKill").getAsInt(),
                        parsedObject.get("animalKill").getAsInt(),
                        parsedObject.get("meetEat").getAsInt(),
                        parsedObject.get("meetEat").getAsDouble()
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

}
