package tv.rewinside.banksystem.utils;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import org.bson.Document;
import tv.rewinside.banksystem.BankSystemPlugin;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class MongoManager {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    @Getter
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;

    @Getter
    private MongoCollection<Document> bankPlayers;
    @Getter
    private MongoCollection<Document> bankAccounts;
    @Getter
    private MongoCollection<Document> bankTransactions;

    public boolean isConnected() {
        return this.mongoClient != null;
    }

    public MongoManager connect() {
        if(isConnected())
            return this;

        this.mongoClient = new MongoClient(new ServerAddress(plugin.getSettings().getDatabaseHost(), plugin.getSettings().getDatabasePort()), Arrays.asList(MongoCredential.createCredential(plugin.getSettings().getDatabaseUsername(), plugin.getSettings().getDatabaseName(), this.plugin.getSettings().getDatabasePassword().toCharArray())));
        return this;
    }

    public void disconnect() {
        if (!isConnected())
            return;

        mongoClient.close();
    }

    public void selectCollection() {
        this.mongoDatabase = mongoClient.getDatabase(plugin.getSettings().getDatabaseName());

        this.bankPlayers = mongoDatabase.getCollection("BankPlayers");
        this.bankAccounts = mongoDatabase.getCollection("BankAccounts");
        this.bankTransactions = mongoDatabase.getCollection("BankTransactions");
    }

}
