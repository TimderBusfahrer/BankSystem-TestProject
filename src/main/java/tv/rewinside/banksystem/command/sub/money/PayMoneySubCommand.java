package tv.rewinside.banksystem.command.sub.money;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.misc.UUIDFetcher;

import java.util.UUID;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class PayMoneySubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    public boolean execute(BankPlayer player, String[] args) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.money.pay")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        long moneyAmount;
        UUID targetUUID;

        try {
            moneyAmount = Long.parseLong(args[2]);
        } catch (NumberFormatException e) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du musst eine Zahl angeben. Sytax: /money pay <Spielername> <Geldwert>");
            return true;
        }

        try {
            targetUUID = UUIDFetcher.getUUID(args[1]);
        } catch (Exception e) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Der Spieler existiert nicht in unserer Datenbank!");
            return true;
        }

        BankPlayer target = plugin.getUserManager().getBankPlayer(targetUUID);

        if(target == null || target.getPlayer() == null) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Der Spieler ist nicht online!");
            return true;
        }

        if(player.getPlayer().getUniqueId().equals(target.getPlayer().getUniqueId())) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du kannst dir nicht selber Geld schicken!");
            return true;
        }

        if (!player.hasEnoughMoney(moneyAmount)) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast nicht genug Bargeld!");
            return true;
        }

        if(!plugin.getServer().getOnlinePlayers().contains(target.getPlayer())) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Der Spieler ist nicht online!");
            return true;
        }

        player.removeMoneyAmountAsync(moneyAmount, currentMoney -> {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast " + target.getPlayer().getName() + " " + moneyAmount + " Euro gegeben. Du hast nun noch " + currentMoney + " Euro!");
        });

        target.addMoneyAmountAsync(moneyAmount, currentMoney -> {
            target.getPlayer().sendMessage(plugin.getPrefix() + "Du hast jetzt " + currentMoney + " Euro. " + player.getPlayer().getName() + " hat dir " + moneyAmount + " Euro geschickt!");
        });
        return true;
    }

}
