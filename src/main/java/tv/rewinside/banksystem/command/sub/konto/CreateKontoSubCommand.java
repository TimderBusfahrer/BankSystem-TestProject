package tv.rewinside.banksystem.command.sub.konto;

import org.apache.commons.lang.RandomStringUtils;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.manager.AccountManager;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class CreateKontoSubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final AccountManager accountManager = plugin.getAccountManager();

    public boolean execute(BankPlayer player) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.konto.create")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        int bankNumber = Integer.parseInt(RandomStringUtils.randomNumeric(6));

        while (plugin.getAccountManager().existsBankAccount(bankNumber)) {
            bankNumber = Integer.parseInt(RandomStringUtils.randomNumeric(6));
        }

        accountManager.createBankAccountAsync(new BankAccount(bankNumber, player.getPlayer().getUniqueId(), new Long(0)));
        player.getPlayer().sendMessage(plugin.getPrefix() + "Dir wurde nun ein Konto erstellt. Kontonummer: " + bankNumber);
        return true;
    }

}
