package tv.rewinside.banksystem.command.sub.konto;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.manager.AccountManager;
import tv.rewinside.banksystem.misc.TransactionType;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class AddKontoSubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final AccountManager accountManager = plugin.getAccountManager();

    public boolean execute(BankPlayer player, String[] args) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.konto.add")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        int bankNumber;
        long moneyAmount;

        try {
            bankNumber = Integer.parseInt(args[1]);
            moneyAmount = Long.parseLong(args[2]);
        } catch (NumberFormatException e) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du musst deine Kontonummer und denn Geldbetrag angeben. Syntax: /konto add <Kontonummer> <Geldwert>");
            return true;
        }

        if(!isBankAccountForm(player, bankNumber)) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Das ist nicht deine Kontonummer. Du kannst alle deine Konten mit /konto list einsehen.");
            return true;
        }

        if(player.getMoneyAmount() < moneyAmount) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast nicht genug Bargeld!");
            return true;
        }

        player.removeMoneyAmountAsync(moneyAmount, currentMoney -> {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast " + moneyAmount + " Euro auf dein Konto " + bankNumber + " eingezahlt!");
        });
        accountManager.getBankAccount(player,bankNumber).addMoneyAmount(moneyAmount);
        plugin.getTransactionManager().addTransactionAsync(accountManager.getBankAccount(player, bankNumber), TransactionType.PLUS, moneyAmount);
        return true;
    }

    private boolean isBankAccountForm(BankPlayer player, int bankNumber) {
        for (BankAccount bankAccount : player.getBankAccounts()) {
            if(bankAccount.getBankNumber() == bankNumber) {
                return true;
            }
        }

        return false;
    }

}
