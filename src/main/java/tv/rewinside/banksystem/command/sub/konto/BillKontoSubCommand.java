package tv.rewinside.banksystem.command.sub.konto;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.entities.BankTransaction;
import tv.rewinside.banksystem.manager.AccountManager;
import tv.rewinside.banksystem.misc.ItemBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class BillKontoSubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final AccountManager accountManager = plugin.getAccountManager();

    private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public boolean execute(BankPlayer player, String[] args) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.konto.bill")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        int bankNumber;

        try {
            bankNumber = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du musst deine Kontonummer und denn Geldbetrag angeben. Syntax: /konto bill <Kontonummer>");
            return true;
        }

        if(!isBankAccountForm(player, bankNumber)) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Das ist nicht deine Kontonummer. Du kannst alle deine Konten mit /konto list einsehen.");
            return true;
        }

        List<String> loreText = new ArrayList<>();

        int transactionCount = 0;

        for(BankTransaction transaction : getSortedTransactions(player, bankNumber)) {
            loreText.add("§7Betrag: " + transaction.getMoneyAmount() + " | Aktion: " + transaction.getType().getName() + " | Zeitpunkt: " +  format.format(transaction.getTimestamp()));

            transactionCount++;

            if(transactionCount == 10)
                break;
        }


        ItemStack bill = new ItemBuilder(Material.BOOK).setDisplayName("Kontoauszug - " + bankNumber).setLore(loreText).build();

        player.getPlayer().getInventory().addItem(bill);
        return true;
    }

    private boolean isBankAccountForm(BankPlayer player, int bankNumber) {
        for (BankAccount bankAccount : player.getBankAccounts()) {
            if(bankAccount.getBankNumber() == bankNumber) {
                return true;
            }
        }

        return false;
    }

    private List<BankTransaction> getSortedTransactions(BankPlayer player, int bankNumber) {
        List<BankTransaction> transactions = accountManager.getBankTransactions(accountManager.getBankAccount(player, bankNumber));
        transactions.sort((o1, o2) -> (o1.getTimestamp() > o2.getTimestamp()) ? -1 : ((o1.getTimestamp() == o2.getTimestamp()) ? 0 : 1));

        return transactions;

    }

}
