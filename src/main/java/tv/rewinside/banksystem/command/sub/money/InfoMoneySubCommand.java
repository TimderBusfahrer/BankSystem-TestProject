package tv.rewinside.banksystem.command.sub.money;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class InfoMoneySubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    public boolean execute(BankPlayer player) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.money.info")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        player.getMoneyAmountAsync(currentMoney -> {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast grade " + currentMoney + " Euro.");
        });
        return true;
    }

}
