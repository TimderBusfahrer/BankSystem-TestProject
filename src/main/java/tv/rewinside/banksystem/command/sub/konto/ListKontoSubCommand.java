package tv.rewinside.banksystem.command.sub.konto;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class ListKontoSubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    public boolean execute(BankPlayer player) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.konto.list")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        if (player.getBankAccounts().size() == 0) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast aktuell keine Konten.");
            return true;
        }

        player.getBankAccountsAsync(bankAccounts -> bankAccounts.forEach(bankAccount -> {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Kontonummer: " + bankAccount.getBankNumber() + " | Guthaben: " + bankAccount.getMoneyAmount());
        }));
        return false;
    }

}
