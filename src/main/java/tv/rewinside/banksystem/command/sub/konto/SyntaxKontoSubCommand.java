package tv.rewinside.banksystem.command.sub.konto;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class SyntaxKontoSubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    public boolean execute(BankPlayer player, String[] args) {
        player.getPlayer().sendMessage(plugin.getPrefix() + "/konto create - Erstellt dir ein neues Konto");
        player.getPlayer().sendMessage(plugin.getPrefix() + "/konto delete <Kontonummer> - Löscht ein gewünschtes Konto");
        player.getPlayer().sendMessage(plugin.getPrefix() + "/konto list - Zeigt dir alle deine Konten");
        player.getPlayer().sendMessage(plugin.getPrefix() + "/konto bill <Kontonummer> - Erstellt dir einen Kontoauszug");
        player.getPlayer().sendMessage(plugin.getPrefix() + "/konto add <Kontonummer> <Geldwert> - Zahlt Bargeld auf eines deiner Konto ein");
        player.getPlayer().sendMessage(plugin.getPrefix() + "/konto remove <Kontonummer> <Geldwert> - Zahlt dir Konto Guthaben aus");
        player.getPlayer().sendMessage(plugin.getPrefix() + "/konto pay <Kontonummer> <Kontonummer 2> <Geldwert> - Überweißt einen Geldbetrag von einem Konto auf das andere");
        return true;
    }
}
