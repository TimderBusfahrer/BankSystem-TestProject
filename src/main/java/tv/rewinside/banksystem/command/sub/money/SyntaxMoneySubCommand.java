package tv.rewinside.banksystem.command.sub.money;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankPlayer;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class SyntaxMoneySubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    public boolean execute(BankPlayer player) {
        player.getPlayer().sendMessage(plugin.getPrefix() + "/money info - Zeigt deinen aktuellen Bargeld Betrag");
        player.getPlayer().sendMessage(plugin.getPrefix() + "/money pay <Spielername> <Geldwert> - Sendet einem Spieler einen Teil deines Bargeldes");
        return true;
    }
}
