package tv.rewinside.banksystem.command.sub.konto;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.manager.AccountManager;
import tv.rewinside.banksystem.misc.TransactionType;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class RemoveKontoSubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final AccountManager accountManager = plugin.getAccountManager();

    public boolean execute(BankPlayer player, String[] args) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.konto.remove")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        int bankNumber;
        long moneyAmount;

        try {
            bankNumber = Integer.parseInt(args[1]);
            moneyAmount = Long.parseLong(args[2]);
        } catch (NumberFormatException e) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du musst deine Kontonummer und denn Geldbetrag angeben. Syntax: /konto remove <Kontonummer> <Geldwert>");
            return true;
        }

        if(!isBankAccountForm(player, bankNumber)) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Das ist nicht deine Kontonummer. Du kannst alle deine Konten mit /konto list einsehen.");
            return true;
        }

        if(accountManager.getBankAccount(player, bankNumber).getMoneyAmount() < moneyAmount) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast nicht genug Geld auf deinem Konto!");
            return true;
        }

        accountManager.getBankAccount(player, bankNumber).removeMoneyAmountAsync(moneyAmount, currentMoney -> {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast jetzt noch " + currentMoney + " Euro auf deinem Konto " + bankNumber + ".");
        });
        player.addMoneyAmountAsync(moneyAmount, currentMoney -> {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast jetzt " + currentMoney + " Euro Bargeld bei dir.");
        });
        plugin.getTransactionManager().addTransactionAsync(accountManager.getBankAccount(player, bankNumber), TransactionType.MINUS, moneyAmount);
        return true;
    }

    private boolean isBankAccountForm(BankPlayer player, int bankNumber) {
        for (BankAccount bankAccount : player.getBankAccounts()) {
            if(bankAccount.getBankNumber() == bankNumber) {
                return true;
            }
        }

        return false;
    }

}
