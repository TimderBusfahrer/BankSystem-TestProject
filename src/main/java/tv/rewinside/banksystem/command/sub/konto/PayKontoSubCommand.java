package tv.rewinside.banksystem.command.sub.konto;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.manager.AccountManager;
import tv.rewinside.banksystem.misc.TransactionType;

import java.sql.SQLException;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class PayKontoSubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final AccountManager accountManager = plugin.getAccountManager();

    public boolean execute(BankPlayer player, String[] args) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.konto.pay")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        int bankNumber;
        int bankNumber2;
        long moneyAmount;

        try {
            bankNumber = Integer.parseInt(args[1]);
            bankNumber2 = Integer.parseInt(args[2]);
            moneyAmount = Long.parseLong(args[3]);
        } catch (NumberFormatException e) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du musst deine Kontonummer und denn Geldbetrag angeben. Syntax: /konto pay <Kontonummer> <Kontonummer 2> <Geldwert>");
            return true;
        }

        if(!isBankAccountForm(player, bankNumber) || !isBankAccountForm(player, bankNumber2)) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Das ist nicht deine Kontonummer. Du kannst alle deine Konten mit /konto list einsehen.");
            return true;
        }

        if(accountManager.getBankAccount(player, bankNumber).getMoneyAmount() < moneyAmount) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast nicht genug Geld auf deinem Konto!");
            return true;
        }

        accountManager.getBankAccount(player, bankNumber).removeMoneyAmountAsync(moneyAmount, currentMoney -> {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast " + moneyAmount + " von deinem Konto " + bankNumber + " auf das Konto " + bankNumber2 + " überwiesen!");
        });
        accountManager.getBankAccount(player, bankNumber2).addMoneyAmountAsync(moneyAmount, currentMoney -> {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du hast nun auf deinem Konto " + bankNumber2 + " " + currentMoney + " Euro.");
        });
        plugin.getTransactionManager().addTransactionAsync(accountManager.getBankAccount(player, bankNumber), TransactionType.MINUS, moneyAmount);
        plugin.getTransactionManager().addTransactionAsync(accountManager.getBankAccount(player, bankNumber2), TransactionType.PLUS, moneyAmount);
        return true;
    }

    private boolean isBankAccountForm(BankPlayer player, int bankNumber) {
        for (BankAccount bankAccount : player.getBankAccounts()) {
            if(bankAccount.getBankNumber() == bankNumber) {
                return true;
            }
        }

        return false;
    }

}
