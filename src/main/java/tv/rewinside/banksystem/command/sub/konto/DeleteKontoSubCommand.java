package tv.rewinside.banksystem.command.sub.konto;

import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.entities.BankAccount;
import tv.rewinside.banksystem.entities.BankPlayer;
import tv.rewinside.banksystem.manager.AccountManager;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class DeleteKontoSubCommand {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();
    private final AccountManager accountManager = plugin.getAccountManager();

    public boolean execute(BankPlayer player, String[] args) {
        if(!player.getPlayer().hasPermission("tv.rewinside.banksystem.konto.delete")) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Dazu hast du keine Rechte!");
            return true;
        }

        int bankNumber;

        try {
            bankNumber = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Du musst deine Kontonummer angeben. Syntax: /konto delete <Kontonummer>");
            return true;
        }

        if(!isBankAccountForm(player, bankNumber)) {
            player.getPlayer().sendMessage(plugin.getPrefix() + "Das ist nicht deine Kontonummer. Du kannst alle deine Konten mit /konto list einsehen.");
            return true;
        }

        accountManager.removeBankAccount(accountManager.getBankAccount(player, bankNumber));
        player.getPlayer().sendMessage(plugin.getPrefix() + "Dein Konto mit der Kontonummer '" + bankNumber + "' wurde soebend gelöscht!");
        return true;
    }

    private boolean isBankAccountForm(BankPlayer player, int bankNumber) {
        for (BankAccount bankAccount : player.getBankAccounts()) {
            if(bankAccount.getBankNumber() == bankNumber) {
                return true;
            }
        }

        return false;
    }

}
