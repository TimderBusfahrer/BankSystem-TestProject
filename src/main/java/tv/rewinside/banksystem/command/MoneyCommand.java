package tv.rewinside.banksystem.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.command.sub.money.InfoMoneySubCommand;
import tv.rewinside.banksystem.command.sub.money.PayMoneySubCommand;
import tv.rewinside.banksystem.command.sub.money.SyntaxMoneySubCommand;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class MoneyCommand implements CommandExecutor {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player))
            return true;

        final Player player = (Player) commandSender;

        if(args.length == 1 && args[0].equalsIgnoreCase("info")) {

            return new InfoMoneySubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()));
        } else if(args.length == 3 && args[0].equalsIgnoreCase("pay")) {

            return new PayMoneySubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()), args);
        } else {

            return new SyntaxMoneySubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()));
        }
    }
}
