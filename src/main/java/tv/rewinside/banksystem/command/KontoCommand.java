package tv.rewinside.banksystem.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tv.rewinside.banksystem.BankSystemPlugin;
import tv.rewinside.banksystem.command.sub.konto.AddKontoSubCommand;
import tv.rewinside.banksystem.command.sub.konto.BillKontoSubCommand;
import tv.rewinside.banksystem.command.sub.konto.CreateKontoSubCommand;
import tv.rewinside.banksystem.command.sub.konto.DeleteKontoSubCommand;
import tv.rewinside.banksystem.command.sub.konto.ListKontoSubCommand;
import tv.rewinside.banksystem.command.sub.konto.PayKontoSubCommand;
import tv.rewinside.banksystem.command.sub.konto.RemoveKontoSubCommand;
import tv.rewinside.banksystem.command.sub.konto.SyntaxKontoSubCommand;

/**
 * Created by TimderBusfahrer
 * Copyright (C) Tim Auf dem Kampe - All Rights Reserved
 **/

public class KontoCommand implements CommandExecutor {

    private final BankSystemPlugin plugin = BankSystemPlugin.getInstance();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player))
            return true;

        final Player player = (Player) commandSender;

        if(args.length == 1 && args[0].equalsIgnoreCase("create")) {

            return new CreateKontoSubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()));
        } else if(args.length == 2 && args[0].equalsIgnoreCase("delete")) {

            return new DeleteKontoSubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()), args);
        } else if(args.length == 1 && args[0].equalsIgnoreCase("list")) {

            return new ListKontoSubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()));
        } else if(args.length == 2 && args[0].equalsIgnoreCase("bill")) {

            return new BillKontoSubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()), args);
        } else if(args.length == 3 && args[0].equalsIgnoreCase("add")) {

            return new AddKontoSubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()), args);
        } else if(args.length == 3 && args[0].equalsIgnoreCase("remove")) {

            return new RemoveKontoSubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()), args);
        } else if(args.length == 4 && args[0].equalsIgnoreCase("pay")) {

            return new PayKontoSubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()), args);
        } else {

            return new SyntaxKontoSubCommand().execute(plugin.getUserManager().getBankPlayer(player.getUniqueId()), args);
        }
    }
}
